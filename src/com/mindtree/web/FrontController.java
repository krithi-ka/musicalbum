package com.mindtree.web;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mindtree.dao.MusicAlbumDao;
import com.mindtree.dao.MusicAlbumDaoImpl;
import com.mindtree.entity.Artist;
import com.mindtree.entity.MusicAlbum;
import com.mindtree.exception.DaoException;

/**
 * Servlet implementation class FrontController
 */
//@WebServlet("/FrontController")
public class FrontController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public FrontController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		if(request.getRequestURI().endsWith("addAlbum.action"))
		{
			MusicAlbumDao dao = new MusicAlbumDaoImpl();
			try
			{
				List<Artist> list = dao.getAllArtists();
				request.setAttribute("artists", list);
			}
			catch(DaoException e)
			{
				e.printStackTrace();
				request.setAttribute("message", e.getMessage());
			}
			request.getRequestDispatcher("addMusicAlbum.jsp").forward(request, response);
		}
		else if(request.getRequestURI().endsWith("addMusic.action"))
		{
			MusicAlbumDao dao = new MusicAlbumDaoImpl();
			
			MusicAlbum album = new MusicAlbum();
			
			album.setAlbumId(0);
			album.setTitle(request.getParameter("title"));
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			try {
				album.setReleaseDate(sdf.parse(request.getParameter("releaseDate")));
			} catch (ParseException e) {
				e.printStackTrace();
				request.setAttribute("message", e.getMessage());
			}
			
			List<Artist> arts = new ArrayList<Artist>();
			String[] art = request.getParameterValues("artistId");
			for(int i=0;i<art.length;i++)
				arts.add(new Artist(Integer.parseInt(art[i])));
			
			album.setArtists(arts);
			
			try {
				dao.addMusicAlbum(album);
			} catch (DaoException e) {
				e.printStackTrace();
				request.setAttribute("message", e.getMessage());
			}
			request.setAttribute("message", "album added successfully");
			request.getRequestDispatcher("index.jsp").forward(request, response);
			
		}
		else if(request.getRequestURI().endsWith("searchAlbum.action"))
		{
			request.getRequestDispatcher("searchInput.jsp").forward(request, response);
		}
		else if(request.getRequestURI().endsWith("searchResult.action"))
		{
			MusicAlbumDao dao = new MusicAlbumDaoImpl();
			List<MusicAlbum> albums = null;
			String title = request.getParameter("title");
			request.setAttribute("search", title);
			
			try {
				albums = dao.getMatchingAlbums(title);
			} 
			catch (DaoException e)
			{
				e.printStackTrace();
				request.setAttribute("message", e.getMessage());
			}
			if(albums.size() == 0)
				request.setAttribute("message", "no such albums");
			else
				request.setAttribute("musicAlbums", albums);
			request.getRequestDispatcher("searchResult.jsp").forward(request, response);
		}
		else if(request.getRequestURI().endsWith("searchArt.action"))
		{
			MusicAlbumDao dao = new MusicAlbumDaoImpl();
			List<MusicAlbum> albums = null;
			String artist = request.getParameter("artist");
			request.setAttribute("search", artist);
			
			try {
				albums = dao.getAlbumForArtist(artist);
			} 
			catch (DaoException e)
			{
				e.printStackTrace();
				request.setAttribute("message", e.getMessage());
			}
			if(albums.size() == 0)
				request.setAttribute("message", "no such albums");
			else
				request.setAttribute("musicAlbums", albums);
			request.getRequestDispatcher("searchResult.jsp").forward(request, response);
		}
			
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
