package com.mindtree.exception;

public class DaoException extends Exception
{

	public DaoException() {
		super();
		
	}
	/**
	 * 
	 * @param message
	 * @param cause
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
		
	}
	
	/**
	 * 
	 * @param message
	 */

	public DaoException(String message) {
		super(message);
		
	}
	
}
