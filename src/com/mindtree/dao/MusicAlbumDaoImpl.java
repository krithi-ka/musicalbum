package com.mindtree.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.mindtree.entity.Artist;
import com.mindtree.entity.MusicAlbum;
import com.mindtree.exception.DaoException;

public class MusicAlbumDaoImpl implements MusicAlbumDao {

	@Override
	public List<Artist> getAllArtists() throws DaoException {
		List<Artist> artists = new ArrayList<Artist>();
		Connection con = DBUtil.getConnection();
		ResultSet rs = null;
		Statement ps = null;

		try {
			ps = con.createStatement();
			rs = ps.executeQuery("select * from artists");
			while (rs.next()) {
				Artist a = new Artist();
				a.setArtistId(rs.getInt("ARTIST_ID"));
				a.setName(rs.getString("NAME"));
				artists.add(a);
			}

		} catch (SQLException e) {
			throw new DaoException("unable to get artists");
		} finally {
			DBUtil.releaseResource(con);
			DBUtil.releaseResource(ps);
			DBUtil.releaseResource(rs);
		}

		return artists;
	}

	@Override
	public void addMusicAlbum(MusicAlbum musicAlbum) throws DaoException {

		Connection con = DBUtil.getConnection();
		PreparedStatement ps = null;
		PreparedStatement link = null;
		ResultSet rs = null;
		String query = "insert into music_album(title,album_relase_date) values (?,?)";
		String query1 = "insert into album_artist(artist_id,album_id) values(?,?)";
		try {
			ps = con.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
			link = con.prepareStatement(query1);
			ps.setString(1, musicAlbum.getTitle());
			ps.setDate(2, new java.sql.Date(musicAlbum.getReleaseDate()
					.getTime()));
			ps.executeUpdate();

			rs = ps.getGeneratedKeys();
			int albumId = -1;
			if (rs.next()) {
				albumId = rs.getInt(1);
			}
			for (Artist artist : musicAlbum.getArtists()) {
				link.setInt(1, artist.getArtistId());
				link.setInt(2, albumId);
				link.executeUpdate();
			}

		} catch (SQLException e) {
			throw new DaoException("unable to add", e);
		}

		finally {
			DBUtil.releaseResource(con);
			DBUtil.releaseResource(ps);
			DBUtil.releaseResource(rs);
		}

	}

	@Override
	public List<MusicAlbum> getMatchingAlbums(String titleStr)
			throws DaoException {

		List<MusicAlbum> albums = new ArrayList<MusicAlbum>();
		Connection con = DBUtil.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		String query = "select album_id,title,album_relase_date from music_album where title like '%"
				+ titleStr + "%'";

		try {
			ps = con.prepareStatement(query);
			rs = ps.executeQuery();
			while (rs.next()) {
				MusicAlbum album = new MusicAlbum();
				album.setAlbumId(rs.getInt(1));
				album.setTitle(rs.getString(2));
				album.setReleaseDate(rs.getDate(3));
				List<Artist> aritsts = getArtistsForAlbum(con, album);
				album.setArtists(aritsts);
				albums.add(album);
			}
		} catch (SQLException e) {
			throw new DaoException("unable to retrieve");
		}

		finally {
			DBUtil.releaseResource(con);
			DBUtil.releaseResource(ps);
			DBUtil.releaseResource(rs);
		}
		return albums;
	}

	private List<Artist> getArtistsForAlbum(Connection con, MusicAlbum album)
			throws SQLException {

		Set<Artist> list = new TreeSet<>();
		Statement statement = null;

		statement = con.createStatement();
		ResultSet rs = statement
				.executeQuery("select art.ARTIST_ID, NAME from ARTISTS art Join ALBUM_ARTIST aa where art.ARTIST_ID = aa.ARTIST_ID and aa.ALBUM_ID = "
						+ album.getAlbumId() + ";");
		while (rs.next()) {
			Artist artist = new Artist(rs.getInt("ARTIST_ID"),
					rs.getString("NAME"));
			list.add(artist);
			System.out.println(artist.getName());
		}
		System.out.println(list);
		return new ArrayList<Artist>(list);
	}

	@Override
	public List<MusicAlbum> getAlbumForArtist(String artist)
			throws DaoException {

		List<MusicAlbum> albums = new ArrayList<MusicAlbum>();
		Connection con = DBUtil.getConnection();
		Statement ps = null;
		ResultSet rs = null;
		String query = "select a.album_id,title, album_relase_date from music_album a ,album_artist am where a.album_id = am.album_id and am.artist_id in (select artist_id from artists where name = '"
				+ artist + "');";

		try {
			ps = con.createStatement();
			rs = ps.executeQuery(query);
			while (rs.next()) {
				MusicAlbum album = new MusicAlbum();
				album.setAlbumId(rs.getInt(1));
				album.setTitle(rs.getString(2));
				album.setReleaseDate(rs.getDate(3));
				List<Artist> aritsts = getArtistsForAlbum(con, album);
				album.setArtists(aritsts);
				albums.add(album);
			}
		} catch (SQLException e) {
			throw new DaoException("unable to retrieve", e);
		} finally {
			DBUtil.releaseResource(con);
			DBUtil.releaseResource(ps);
			DBUtil.releaseResource(rs);
		}
		return albums;
	}

}