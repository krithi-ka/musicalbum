package com.mindtree.dao;

import java.util.List;
import com.mindtree.entity.*;
import com.mindtree.exception.*;

/**
 * 
 * @author M1027304
 *
 */
public interface MusicAlbumDao {
	/**
	 * 
	 * @return artists
	 * @throws DaoException
	 */
	public List<Artist> getAllArtists() throws DaoException;

	/**
	 * 
	 * @param musicAlbum album to store
	 * @throws DaoException 
	 */
	public void addMusicAlbum(MusicAlbum musicAlbum) throws DaoException;
	
	/**
	 * 
	 * @param titleStr partial title string 
	 * @return matching music albums for a string
	 * @throws DaoException
	 */
	public List<MusicAlbum> getMatchingAlbums(String titleStr) throws DaoException;
	
	/**
	 * 
	 * @param artist
	 * @return list of music album for a given artist
	 * @throws DaoException
	 */
	public List<MusicAlbum> getAlbumForArtist(String artist) throws DaoException;
}
