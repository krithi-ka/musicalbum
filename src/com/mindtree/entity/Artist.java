package com.mindtree.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author M1027304
 *
 */
public class Artist implements Comparable<Artist>{
	
	private int artistId;
	private String name;
	private List<MusicAlbum> albums = new ArrayList<MusicAlbum>();
	
	public Artist(){
		
	}
	/**
	 * 
	 * @param artistId
	 * @param name
	 */
	
	public Artist(int artistId)
	{
		this.artistId = artistId;
	}
	/**
	 * 
	 * @param artistId
	 * @param name
	 */
	public Artist(int artistId, String name) {
		super();
		this.artistId = artistId;
		this.name = name;
	}
	/**
	 * @return the artistId
	 */
	public int getArtistId() {
		return artistId;
	}
	/**
	 * @param artistId the artistId to set
	 */
	public void setArtistId(int artistId) {
		this.artistId = artistId;
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the albums
	 */
	public List<MusicAlbum> getAlbums() {
		return albums;
	}
	/**
	 * @param albums the albums to set
	 */
	public void setAlbums(List<MusicAlbum> albums) {
		this.albums = albums;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + artistId;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Artist other = (Artist) obj;
		if (artistId != other.artistId)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}
	
	@Override
	public int compareTo(Artist o) {
		return this.name.compareTo(o.name);
	}
}
