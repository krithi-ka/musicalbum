package com.mindtree.entity;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 
 * @author M1027304
 *	music album 
 */
public class MusicAlbum {
	
	private int albumId;
	private String title;
	private Date releaseDate;
	private List<Artist> artists = new ArrayList<Artist>();
	
	public MusicAlbum(){
		
	}

	/**
	 * @param albumId
	 * @param title
	 * @param releaseDate
	 */
	public MusicAlbum(int albumId, String title, Date releaseDate) {
		super();
		this.albumId = albumId;
		this.title = title;
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the albumId
	 */
	public int getAlbumId() {
		return albumId;
	}

	/**
	 * @param albumId the albumId to set
	 */
	public void setAlbumId(int albumId) {
		this.albumId = albumId;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the releaseDate
	 */
	public Date getReleaseDate() {
		return releaseDate;
	}

	/**
	 * @param releaseDate the releaseDate to set
	 */
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	/**
	 * @return the artists
	 */
	public List<Artist> getArtists() {
		return artists;
	}

	/**
	 * @param artists the artists to set
	 */
	public void setArtists(List<Artist> artists) {
		this.artists = artists;
	}
	
	
	
}
