<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add Music Album</title>
<link rel = "stylesheet" type = "text/css" href = "css/style.css">
<script type="text/javascript" src = "scripts/musicAlbum.js"></script>
</head>
<body>
	<div id="messageDiv">
		${requestScope.message} ${param.message}</div>
	<h2>Add a new Music Album</h2>
	<form name = "myForm" method="get" action="addMusic.action">
		<table>
			<tr>
				<td>Music album Title</td>
				<td><input id = "title" type="text" name="title" /></td>
			</tr>
			<tr>
				<td>Artists</td>
				<td><select name="artistId" multiple="multiple">
				
						<c:set var = "list" value = "${requestScope.artists}"/>
						<c:forEach var = "artist" items = "${list}">
						
							<option value="${artist.artistId}">${artist.name}</option>
						</c:forEach>
				</select></td>
			</tr>
			<tr>
				<td>Release date [yyyy-mm-dd]</td>
				<td><input id = "date" type="text" name="releaseDate" /></td>
			</tr>
				<tr>
				<td><input type="submit" value="Add a new Music Album" onclick = "validate()"/></td>
				<td><input type="button" value="Cancel" onclick="window.location.href='index.jsp'"/></td>
			</tr>
		</table>
	</form>
</body>
</html>