<%@page import="com.mindtree.entity.Artist"%>
<%@page import="com.mindtree.entity.MusicAlbum"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Results</title>
<link rel="stylesheet" type = "text/css" href = "css/style.css">
</head>
<body>
	Search Results for ${search}<br/> <br/><br/>
	
	<div id="messageDiv">
		${requestScope.message} </div>
	
	<c:set var = "musicAlbums" value = "${requestScope.musicAlbums}"/>
	
	<c:forEach var = "album" items = "${musicAlbums}"> 
		Album ID:
		${album.albumId}
		<br /> Album Title:
		${album.title}
		<br /> Release Date:
		${album.releaseDate}
		<br /> Artists:
		<br />
		<c:set var = "artists" value = "${album.artists}"/>
		<c:forEach var = "artist" items = "${artists}">
			${artist.name}
		</c:forEach>
		<br />
		<hr />
	</c:forEach>
</body>
</html>