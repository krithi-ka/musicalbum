/**
 * M1027304
 */

function validate()
{
	var title = document.getElementById("title").value;
	var date = document.getElementById("date").value;

	if(title == null || title == "")
	{
		alert("Enter a valid title");
		return false;
	}
	else if(date == null || date == "")
	{
		alert("Enter a valid date");
		return false;
	}
}