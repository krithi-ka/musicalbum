<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Search Music Album</title>
<link rel = "stylesheet" type = "text/css" href = "css/style.css"> 
</head>
<body>
	<div id="messageDiv">
		${requestScope.message} ${param.message}</div>
	<h2>Search Music Album</h2>
	<form method="get" action="searchResult.action">
		<table>
			<tr>
				<td>Music album Title</td>
				<td><input type="text" name="title" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Search Music Album" /></td>
				<td><input type="button" value="Cancel"
					onclick="window.location.href='index.jsp'" /></td>
			</tr>
		</table>
	</form>
	<form method="get" action="searchArt.action">
		<table>
			<tr>
				<td>Artist Name</td>
				<td><input type="text" name="artist" /></td>
			</tr>
			<tr>
				<td><input type="submit" value="Search Music Album" /></td>
				<td><input type="button" value="Cancel"
					onclick="window.location.href='index.jsp'" /></td>
			</tr>
		</table>
	</form>
</body>
</html>