<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Music Album</title>
<link rel = "stylesheet" type = "text/css" href = "css/style.css">
</head>
<body>
	<h3> Music Album </h3>
	
	<div id = "content">	
	<a href="addAlbum.action">Add a new Music album</a> <br />
	<a href="searchAlbum.action">Search for an Album</a> <br />
	</div>
	
	<div id="messageDiv">
		${requestScope.message} ${param.message}</div>
		
</body>
</html>