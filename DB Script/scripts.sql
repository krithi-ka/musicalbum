create database MUSIC_ALBUM_DB;
use MUSIC_ALBUM_DB;

create table MUSIC_ALBUM (album_id int primary key auto_increment, title varchar(150),album_relase_date date);

create table ARTISTS (ARTIST_ID int primary key auto_increment, NAME varchar(50));


create table ALBUM_ARTIST ( ALBUM_ID int, ARTIST_ID int,
constraint ALBUM_ARTIST_PK primary key(ALBUM_ID, ARTIST_ID ),
constraint MUSIC_ALBUM_FK foreign key (ALBUM_ID) references MUSIC_ALBUM (ALBUM_ID),
constraint ARTIST_FK foreign key (ARTIST_ID) references ARTISTS (ARTIST_ID)
);


insert into ARTISTS values (1, 'Alister Griffin');
insert into ARTISTS values (2, 'Robin Gibb');
insert into ARTISTS values (3, 'Brian Wilson');
insert into ARTISTS values (4, 'Van Dyke Parks');
insert into ARTISTS values (5, 'Terry Melcher');
insert into ARTISTS values (6, 'Rihanna');
insert into ARTISTS values (7, 'Eminem');
insert into ARTISTS values (8, 'Adele');

insert into MUSIC_ALBUM values (1, 'Still Waters', '1997-2-1');
insert into MUSIC_ALBUM values (2, 'This Is Where I Came In', '2000-11-5');
insert into MUSIC_ALBUM values (3, 'Summer in Paradise', '1992-10-30');
insert into MUSIC_ALBUM values (4, 'Summer', '2014-5-11');
insert into MUSIC_ALBUM values (5, 'Paradise', '2012-10-21');

insert into ALBUM_ARTIST values (1, 1);
insert into ALBUM_ARTIST values (1, 2);
insert into ALBUM_ARTIST values (2, 2);
insert into ALBUM_ARTIST values (2, 3);
insert into ALBUM_ARTIST values (3, 3);
insert into ALBUM_ARTIST values (3, 4);
insert into ALBUM_ARTIST values (3, 5);
insert into ALBUM_ARTIST values (4, 6);
insert into ALBUM_ARTIST values (5, 7);
insert into ALBUM_ARTIST values (5, 8);